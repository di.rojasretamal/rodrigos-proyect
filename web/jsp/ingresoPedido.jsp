<%-- 
    Document   : ingresoCliente
    Created on : 02-11-2017, 21:41:32
    Author     : Nicolas
--%>

<%@page import="java.util.List"%>
<%@page import="project.entidad.Carretera"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso carretera</title>
        <style>
            .tituloPrincipal{
                font-size: 25px;
                margin-bottom: 20px;
                text-align: center;
            }

            input{
                margin: 5px;
            }

            .buttonSeleccionar{
                border: none;
                width: 200px;
                height: 60px;
                font-size: 20px;
                margin: 10px;
                background-color: khaki;
            }

            .buttonSeleccionar:hover{
                background-color: yellow;
                border-style: solid;
                border-width: 5px;
            }

            .imagenCarro{
                background-image: url(img/estacionamiento.jpg);
                width: 150px;
                height: 150px;
                background-repeat: no-repeat;
            }

            .menuLateralIzquierdo{
                float: left;
                width: 25%;
            }

            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }

            td{
                text-align: center;
            }
        </style>  
    </head>
    <body>
        <div class="tituloPrincipal"> Ingreso Carretera </div>
        <div class="menuLateralIzquierdo">                
            <div class="imagenCarro"></div>                
            <a href="ingresoPedido.jsp">Inicio</a><br/>
            <a href="buscarPedidos.jsp">Ver pedidos</a><br/>
            <a href="http://www.google.cl">Ayuda</a><br/> 
            <p>Opciones de Pago</p>
            <form>
                <input type="radio" name="pago" value="Transferencia">Transferencia<br>
                <input type="radio" name="pago" value="PagoLinea">Pago Linea<br>
                <input type="radio" name="pago" value="OrdenCompra">Orden de Compra
            </form>
            <p>Opciones de Retiro</p>
            <form>
                <input type="radio" name="opcionEnvio" value="correoElectronico">Oficina<br>
                <input type="radio" name="opcionEnvio" value="direccionParticular">Envio
            </form>
        </div>
        <div style="float:left; width: 49%;">
            <form action="/boucher.jsp" method="GET">
                <!--<form name="formSand" method="post" action="IngresarCliente.do">-->
                <div><label>Rut        : </label><input type="number" name="idRut" required="required"/></div>
                <div><label>Nombre     : </label><input type="text" name="idNombre" required="required"/></div>
                <div><label>Direccion   : </label><input type="text" name="idDireccion" required="required"/></div>
                <div><label>Comprado por      : </label><input type="text" name="idEmail" required="required"/></div>
                <!--<input type="submit" value="Registrar Cliente" class="buttonSeleccionar"><br>-->
                <!--</form>-->
                <p>Seleccione Carretera, indique la cantidad de dinero y agregue pedido.</p>
                <select>
                    <option value="Estacionamiento">Carretera</option>
                    <option value="Estacionamiento">Estacionamiento</option>
                </select>
                <table style="width:100%; margin-top: 20px;">
                    <tr>
                        <th>Carretera</th>
                        <th>Cantidad</th> 
                        <th></th>
                    </tr>
                    <tr>
                        <td>Plaza valparaiso</td>
                        <td><input type="number" name="cantidadVal" required="required" value="15000"></td> 
                        <td><button id="modificar" value="Modificar">Modificar</button></td>
                    </tr>
                    <tr>
                        <td>Otro Lugar</td>
                        <td><input type="number" name="cantidadVal" required="required" value="2000"></td>
                        <td><button id="modificar">Modificar</button></td>
                    </tr>
                </table>

                <input class="buttonSeleccionar" name="botonSeleccionar" type="submit" value="Pagar">
            </form>

        </div>
        <div style="float: left; width: 25%;">
            <a href="buscarPedidos.jsp">Ver Carretera</a>
        </div>    
    </body>
</html>

