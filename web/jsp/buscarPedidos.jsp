<%-- 
    Document   : ingresoCliente
    Created on : 02-11-2017, 21:41:32
    Author     : Nicolas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AutoPark</title>
        <style>
            .tituloPrincipal{
                font-size: 25px;
                margin-bottom: 20px;
                text-align: center;
            }

            input{
                margin: 5px;
            }

            .buttonSeleccionar{
                border: none;
                width: 200px;
                height: 60px;
                font-size: 20px;
                margin: 10px;
                background-color: khaki;
            }

            .buttonSeleccionar:hover{
                background-color: yellow;
                border-style: solid;
                border-width: 5px;
            }

            .imagenCarro{
                background-image: url(img/estacionamiento.jpg);
                width: 150px;
                height: 150px;
                background-repeat: no-repeat;
            }

            .menuLateralIzquierdo{
                float: left;
                width: 25%;
            }

            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
        </style>  
    </head>
    <body>
        <div class="tituloPrincipal"> Datos Empresa </div>
        <div class="menuLateralIzquierdo">                
            <div class="imagenCarro"></div>                
            <a href="ingresoPedido.jsp">Inicio</a><br/>
            <a href="buscarPedidos.jsp">Ver pedidos</a><br/>
            <a href="http://www.google.cl">Ayuda</a><br/>      
        </div>
        <div style="float:left; width: 49%;">            
            <form >
                <div><label>Rut        : </label><input type="number" name="idRut" required="required"/></div>       
                <input name="botonSeleccionar" type="submit" value="Buscar">          
            </form>
            <table style="width:100%; margin-top: 20px;">
                <tr>
                    <th>Pedido</th>
                    <th>Total</th> 
                    <th>Pedir</th>
                </tr>
                <tr>
                    <td>Ruta 68</td>
                    <td>2000</td> 
                    <td><button id="modificar">Pedir</button></td>
                </tr>
                <tr>
                    <td>Ruta 67</td>
                    <td>2000</td> 
                    <td><button id="modificar">Pedir</button></td>
                </tr>
                <tr>
                    <td>Ruta 55</td>
                    <td>2000</td> 
                    <td><button id="modificar">Pedir</button></td>
                </tr>
                <tr>
                    <td>Carretera del Sol</td>
                    <td>2000</td> 
                    <td><button id="modificar">Pedir</button></td>
                </tr>
            </table>              
        </div>
        <div style="float: left; width: 25%;">
            <a href="buscarPedidos.jsp">Ver Carretera</a>
        </div>    
    </body>
</html>

