<%-- 
    Document   : ingresoCliente
    Created on : 02-11-2017, 21:41:32
    Author     : Nicolas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Boucher Carretera</title>
        <style>
            .tituloPrincipal{
                font-size: 25px;
                margin-bottom: 20px;
                text-align: center;
            }

            input{
                margin: 5px;
            }

            .buttonSeleccionar{
                border: none;
                width: 200px;
                height: 60px;
                font-size: 20px;
                margin: 10px;
                background-color: khaki;
            }

            .buttonSeleccionar:hover{
                background-color: yellow;
                border-style: solid;
                border-width: 5px;
            }

            .imagenCarro{
                background-image: url(img/estacionamiento.jpg);
                width: 150px;
                height: 150px;
                background-repeat: no-repeat;
            }

            .menuLateralIzquierdo{
                float: left;
                width: 25%;
            }

            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
        </style>  
    </head>
    <body>
        <div class="tituloPrincipal"> Boucher Carretera </div>
        <div class="menuLateralIzquierdo">                
            <div class="imagenCarro"></div>                
            <a href="ingresoPedido.jsp">Inicio</a><br/>
            <a href="buscarPedidos.jsp">Ver pedidos</a><br/>
            <a href="http://www.google.cl">Ayuda</a><br/>            
        </div>
        <div style="float:left; width: 49%;">
            <p>Pedido numero XXXX</p>
            
            <table style="width:100%; margin-top: 20px;">
                <tr>
                    <th>Estacionamiento</th>
                    <th>Monto</th> 
                    <th>Numero Ticket</th>
                    <th></th>
                </tr>
                <tr>
                    <td>Plaza valparaiso</td>
                    <td>2000</td> 
                    <td>234234234</td>
                    <td>Age</td>
                </tr>
                <tr>
                    <td>Otro Lugar</td>
                    <td>2000</td> 
                    <td>234234234</td>
                    <td>Age</td>
                </tr>
                <tr>
                    <td>Otro Lugar</td>
                    <td>2000</td> 
                    <td>234234234</td>
                    <td>Age</td>
                </tr>
                <tr>
                    <td>Otro Lugar</td>
                    <td>2000</td> 
                    <td>234234234</td>
                    <td>Age</td>
                </tr>
            </table>            
            <h3>Total a Pagar: <b>$ 8.000</b></h3>            
            <h3>Opcion de Envio : X X X X X X X X </h3>
            <br/>
            <p style="float: right;">Muchas gracias por preferirnos</p>            
        </div>
        <div style="float: left; width: 25%;">
            <a href="buscarPedidos.jsp">Ver Carretera</a>
        </div>    
    </body>
</html>

