/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import project.entidad.Carretera;
import project.utils.ConnectionFactory;

/**
 *
 * @author Diego
 */
public class CarreteraDAO {

    private final static Logger logger = Logger.getLogger(CarreteraDAO.class.getName());
    
    public List<Carretera> listarCarretera() {

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        Carretera carretera = new Carretera();
        List<Carretera> listaCarretera = new ArrayList();

        try {
            conn = ConnectionFactory.getConnection();
            
            sql = "SELECT idCarretera, nombreCarretera FROM carretera; ";
            rs = st.executeQuery(sql);
            
             while (rs.next()){
                 carretera.setIdCarretera(rs.getInt("idCarretera"));
                 carretera.setNombreCarretera(rs.getString("nombreCarretera"));
                 
                 listaCarretera.add(carretera);
             }
            
            ConnectionFactory.closeConnection(conn);
            
            return listaCarretera;

        } catch (Exception e) {
            logger.info("Error para seleccionar carretera" + e);
            throw new RuntimeException(e);
        }
    }
    
    public Carretera listarCarretera(int id) {

        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        Carretera carretera = new Carretera();

        try {
            conn = ConnectionFactory.getConnection();            
            sql = "SELECT idCarretera, nombreCarretera FROM carretera WHERE idCarretera = ?; ";
            st = conn.prepareStatement(sql);
            st.setInt(1, id);            
            rs = st.executeQuery(sql);
            
             while (rs.next()){
                 carretera.setIdCarretera(rs.getInt("idCarretera"));
                 carretera.setNombreCarretera(rs.getString("nombreCarretera").trim());               
             }
            
            ConnectionFactory.closeConnection(conn);
            
            return carretera;

        } catch (Exception e) {
            logger.info("Error para seleccionar carretera" + e);
            throw new RuntimeException(e);
        }
    }
}
