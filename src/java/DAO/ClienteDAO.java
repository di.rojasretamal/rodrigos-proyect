/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import project.entidad.Carretera;
import project.utils.ConnectionFactory;

/**
 *
 * @author Diego
 */
public class ClienteDAO {

    private final static Logger logger = Logger.getLogger(ClienteDAO.class.getName());

    public boolean existePorRut(int rut) {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        Carretera carretera = new Carretera();
        List<Carretera> listaCarretera = new ArrayList();
        boolean existe = false;

        try {
            conn = ConnectionFactory.getConnection();            
            sql = "SELECT idCliente, nombre, rut, nombrePersonaViaje, Pago_idPago, Retiro_idRetiro FROM cliente WHERE rut = ?; ";
            st = conn.prepareStatement(sql);
            st.setInt(1, rut);
            rs = st.executeQuery(sql);
            
             while (rs.next()){
                 carretera.setIdCarretera(rs.getInt("idCarretera"));
                 carretera.setNombreCarretera(rs.getString("nombreCarretera"));
                 listaCarretera.add(carretera);
             }
             
             if(!listaCarretera.isEmpty()){
                 existe = true;
             }
            
            ConnectionFactory.closeConnection(conn);
            
            return existe;

        } catch (Exception e) {
            logger.info("Error para seleccionar carretera" + e);
            throw new RuntimeException(e);
        }
    }
}
