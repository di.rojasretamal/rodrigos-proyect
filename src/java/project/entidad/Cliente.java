/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entidad;

/**
 *
 * @author Diego
 */
public class Cliente {
    int idCliente;
    String nombre;
    String rut;
    String nombrePersonaViaje;
    int pago_idPago;
    int retiro_idRetiro;

    public Cliente(int idCliente, String nombre, String rut, String nombrePersonaViaje, int pago_idPago, int retiro_idRetiro) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.rut = rut;
        this.nombrePersonaViaje = nombrePersonaViaje;
        this.pago_idPago = pago_idPago;
        this.retiro_idRetiro = retiro_idRetiro;
    }

    public Cliente() {
        this.idCliente = 0;
        this.nombre = "";
        this.rut = "";
        this.nombrePersonaViaje = "";
        this.pago_idPago = 0;
        this.retiro_idRetiro = 0;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombrePersonaViaje() {
        return nombrePersonaViaje;
    }

    public void setNombrePersonaViaje(String nombrePersonaViaje) {
        this.nombrePersonaViaje = nombrePersonaViaje;
    }

    public int getPago_idPago() {
        return pago_idPago;
    }

    public void setPago_idPago(int pago_idPago) {
        this.pago_idPago = pago_idPago;
    }

    public int getRetiro_idRetiro() {
        return retiro_idRetiro;
    }

    public void setRetiro_idRetiro(int retiro_idRetiro) {
        this.retiro_idRetiro = retiro_idRetiro;
    }
    
    
}
