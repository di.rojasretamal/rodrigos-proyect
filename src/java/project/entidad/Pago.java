/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entidad;

/**
 *
 * @author Diego
 */
public class Pago {
    int idPago;
    String tipoPago;
    int boucher_boucherId;

    public Pago(int idPago, String tipoPago, int boucher_boucherId) {
        this.idPago = idPago;
        this.tipoPago = tipoPago;
        this.boucher_boucherId = boucher_boucherId;
    }

    public Pago() {
        this.idPago = 0;
        this.tipoPago = "";
        this.boucher_boucherId = 0;
    }

    public int getIdPago() {
        return idPago;
    }

    public void setIdPago(int idPago) {
        this.idPago = idPago;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public int getBoucher_boucherId() {
        return boucher_boucherId;
    }

    public void setBoucher_boucherId(int boucher_boucherId) {
        this.boucher_boucherId = boucher_boucherId;
    }
}
