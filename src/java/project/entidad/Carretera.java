/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entidad;

/**
 *
 * @author Diego
 */
public class Carretera {
    int idCarretera;
    String nombreCarretera;

    public Carretera(int idCarretera, String nombreCarretera, int pasaje_idPasaje) {
        this.idCarretera = idCarretera;
        this.nombreCarretera = nombreCarretera;
    }
    
    public Carretera(){
        this.idCarretera = 0;
        this.nombreCarretera = "";
    }

    public int getIdCarretera() {
        return idCarretera;
    }

    public void setIdCarretera(int idCarretera) {
        this.idCarretera = idCarretera;
    }

    public String getNombreCarretera() {
        return nombreCarretera;
    }

    public void setNombreCarretera(String nombreCarretera) {
        this.nombreCarretera = nombreCarretera;
    }
}

