/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entidad;

/**
 *
 * @author Diego
 */
public class Pasaje {
    int idPasaje;
    String idCarretera;
    int valor;
    int cantidad;
    int cliente_idCliente;

    public Pasaje(int idPasaje, String idCarretera, int valor, int cantidad, int cliente_idCliente) {
        this.idPasaje = idPasaje;
        this.idCarretera = idCarretera;
        this.valor = valor;
        this.cantidad = cantidad;
        this.cliente_idCliente = cliente_idCliente;
    }

    public Pasaje() {
        this.idPasaje = 0;
        this.idCarretera = "";
        this.valor = 0;
        this.cantidad = 0;
        this.cliente_idCliente = 0;
    }

    public int getIdPasaje() {
        return idPasaje;
    }

    public void setIdPasaje(int idPasaje) {
        this.idPasaje = idPasaje;
    }

    public String getIdCarretera() {
        return idCarretera;
    }

    public void setIdCarretera(String idCarretera) {
        this.idCarretera = idCarretera;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCliente_idCliente() {
        return cliente_idCliente;
    }

    public void setCliente_idCliente(int cliente_idCliente) {
        this.cliente_idCliente = cliente_idCliente;
    }
    
    
    
}
