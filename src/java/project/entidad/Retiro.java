/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entidad;

/**
 *
 * @author Diego
 */
public class Retiro {
    int idRetiro;
    String tipoRetiro;

    public Retiro(int idRetiro, String tipoRetiro) {
        this.idRetiro = idRetiro;
        this.tipoRetiro = tipoRetiro;
    }

    public Retiro() {
        this.idRetiro = 0;
        this.tipoRetiro = "";
    }
    
    public int getIdRetiro() {
        return idRetiro;
    }

    public void setIdRetiro(int idRetiro) {
        this.idRetiro = idRetiro;
    }

    public String getTipoRetiro() {
        return tipoRetiro;
    }

    public void setTipoRetiro(String tipoRetiro) {
        this.tipoRetiro = tipoRetiro;
    }

    
    
}
