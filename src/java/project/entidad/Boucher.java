/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entidad;

/**
 *
 * @author Diego
 */
public class Boucher {
    int idBoucher;
    String totalPago;
    String datosBoucher;

    public Boucher(int idBoucher, String totalPago, String datosBoucher) {
        this.idBoucher = idBoucher;
        this.totalPago = totalPago;
        this.datosBoucher = datosBoucher;
    }
    
    public Boucher()
    {
        this.idBoucher = 0;
        this.totalPago = "";
        this.datosBoucher = "";
    }

    public int getIdBoucher() {
        return idBoucher;
    }

    public void setIdBoucher(int idBoucher) {
        this.idBoucher = idBoucher;
    }

    public String getTotalPago() {
        return totalPago;
    }

    public void setTotalPago(String totalPago) {
        this.totalPago = totalPago;
    }

    public String getDatosBoucher() {
        return datosBoucher;
    }

    public void setDatosBoucher(String datosBoucher) {
        this.datosBoucher = datosBoucher;
    }
    
    
}
