package project.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Logger;

/**
 *
 * @author Diego
 */
public class ConnectionFactory {

    private final static Logger logger = Logger.getLogger(ConnectionFactory.class.getName());

    public static Connection getConnection() {
        Connection conn = null;
        try {
            //conn = DriverManager.getConnection(Constantes.URL.trim(),Constantes.USER.trim(),Constantes.PASS.trim());
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa", "root", "admin");
            
            return conn;
        } catch (Exception e) {
            logger.info("Problemas para ingresar a la base de datos." + e);
            throw new RuntimeException(e);
        }
    }

    public static void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (Exception e) {
            logger.info("Problemas para cerrar a la base de datos." + e);
            throw new RuntimeException(e);
        }
    }
    
    public static void commitConnection(Connection conn) {
        try {
            conn.commit();
        } catch (Exception e) {
            logger.info("Problemas para realizar commit a la base de datos." + e);
            throw new RuntimeException(e);
        }
    }
}
